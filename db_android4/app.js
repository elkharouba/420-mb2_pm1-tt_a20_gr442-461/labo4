const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Point = require('./module/Point');
const User = require('./module/User');
const cors = require('cors');

app.use(bodyParser.json());
app.use(cors());

app.post('/newPoint',(req,res)=>{
   
    const point = new Point(req.body);

    point.save((err,result)=>
    {
        if(err)
        {
            res.status(500).json(err);
        }
        else
        {
            res.status(200).json(result);
        }
    }); 
});

app.post('/newUser',(req,res)=>{
   
    const user = new User(req.body);

    user.save((err,result)=>
    {
        if(err)
        {
            res.status(500).json(err);
        }
        else
        {
            res.status(200).json(result);
        }
    }); 
});



app.delete('/delete/:id',(req,res)=>{

    const id = req.params.id;
    Point.findByIdAndDelete(id, (err,response)=>{
        if(err)
        {
            res.status(500).json(err);
        }
        else
        {
           res.status(200).json(response._id);
        }
    });
});


app.get('/getData',(req,res)=>
{   
    Point.find()
    .then((result)=>{
        res.status(200).json(result);
    })
    .catch(err=> console.log(err));
});

app.get('/getDataUser',(req,res)=>
{   
    User.find()
    .then((result)=>{
        res.status(200).json(result);
    })
    .catch(err=> console.log(err));
});

mongoose.connect('mongodb://127.0.0.1:27017/dblabo',{ useUnifiedTopology: true  , useNewUrlParser: true });


app.listen(4200,()=>{
    console.log("port connected !");
});

