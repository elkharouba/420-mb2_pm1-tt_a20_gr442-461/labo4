const mongoose = require('mongoose');

const pointSchema = new mongoose.Schema({
    temperature : String,
    pression : String,
    humidite : String,
    date : String,
    lutidue : String,
    longitude :String
});


module.exports = mongoose.model('Point',pointSchema);