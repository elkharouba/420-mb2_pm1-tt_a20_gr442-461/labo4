const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
        nom : String,
        prenom :String,
        username:String,
        password:String
});


module.exports = mongoose.model('User',userSchema);