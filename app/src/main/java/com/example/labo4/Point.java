package com.example.labo4;

import java.io.Serializable;
import java.util.Date;

public class Point implements Serializable {


    private  String _id;
    private String Temperature;
    private String Pression;
    private String Humidite;
    private String Date ;
    private String Lutidue;
    private String Longitude;

    public Point() {
    }

    public Point(String _id, String temperature, String pression, String humidite, String date, String lutidue, String longitude) {
        this._id = _id;
        Temperature = temperature;
        Pression = pression;
        Humidite = humidite;
        Date = date;
        Lutidue = lutidue;
        Longitude = longitude;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTemperature() {
        return Temperature;
    }

    public void setTemperature(String temperature) {
        Temperature = temperature;
    }

    public String getPression() {
        return Pression;
    }

    public void setPression(String pression) {
        Pression = pression;
    }

    public String getHumidite() {
        return Humidite;
    }

    public void setHumidite(String humidite) {
        Humidite = humidite;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getLutidue() {
        return Lutidue;
    }

    public void setLutidue(String lutidue) {
        Lutidue = lutidue;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }
}
