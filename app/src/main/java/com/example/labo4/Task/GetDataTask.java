package com.example.labo4.Task;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.labo4.Point;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class GetDataTask extends  AsyncTask<String, Void, String> {





    Context context;
    ProgressDialog progressDialog;

    ArrayList<Point> list;

    public GetDataTask(Context context)
    {
        this.context = context;
        this.list = new ArrayList<Point>();
    }

    @Override
    protected void onPreExecute() {

        super.onPreExecute();

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading data...");
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {

        try {
            return getData(params[0]);
        } catch (IOException ex) {
            return "Network error !";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        //set data response to textView
        //mResult.setText(result);
       /// Toast.makeText(context,result,Toast.LENGTH_LONG).show();

        JSONArray json = null;
        try {
            json = new JSONArray(result.toString());
            for(int i=0; i < json.length(); i++)
            {
                JSONObject equip = json.getJSONObject(i);
                Point p = new Point();
                p.set_id(equip.getString("_id"));
                p.setDate(equip.getString("date"));
                list.add(p);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        System.out.println(result);
        //cancel progress dialog
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public ArrayList<Point>  gettingData()
    {
        return list;
    }

    private String getData(String urlPath) throws IOException {
        StringBuilder result = new StringBuilder();
        BufferedReader bufferedReader =null;

        try {
            //Initialize and config request, then connect to server
            URL url = new URL(urlPath);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(10000 /* milliseconds */);
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("Content-Type", "application/json");// set header
            urlConnection.connect();

            //Read data response from server
            InputStream inputStream = urlConnection.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                result.append(line).append("\n");
            }

        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        }

        return result.toString();
    }


}
