package com.example.labo4.Task;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.example.labo4.Point;
import com.example.labo4.SingleDataActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class GetSingleDataTask extends  AsyncTask<String, Void, String> {


    Context context;
    ProgressDialog progressDialog;

    Point point;

    int id_postion;

    public GetSingleDataTask(Context context, int id)
    {
        this.context = context;

        id_postion = id;
        this.point = new Point();
    }

    @Override
    protected void onPreExecute() {

        super.onPreExecute();

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading data...");
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {

        try {
            return getData(params[0]);
        } catch (IOException ex) {
            return "Network error !";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        //set data response to textView
        //mResult.setText(result);
       /// Toast.makeText(context,result,Toast.LENGTH_LONG).show();

        JSONArray json = null;
        try {
            json = new JSONArray(result.toString());
            for(int i=0; i < json.length(); i++)
            {
                if( i == id_postion)
                {
                    JSONObject equip = json.getJSONObject(i);
                    point.set_id(equip.getString("_id"));
                    point.setLongitude(equip.getString("longitude"));
                    point.setLutidue(equip.getString("lutidue"));
                    point.setHumidite(equip.getString("humidite"));
                    point.setTemperature(equip.getString("temperature"));
                    point.setPression(equip.getString("pression"));
                    point.setDate(equip.getString("date"));
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }



        //cancel progress dialog
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        if(point != null)
        {
            Intent intent = new Intent(context, SingleDataActivity.class);
            intent.putExtra("Object",this.point);
            context.startActivity(intent);
        }
    }


    private String getData(String urlPath) throws IOException {
        StringBuilder result = new StringBuilder();
        BufferedReader bufferedReader =null;

        try {
            //Initialize and config request, then connect to server
            URL url = new URL(urlPath);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(10000 /* milliseconds */);
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("Content-Type", "application/json");// set header
            urlConnection.connect();

            //Read data response from server
            InputStream inputStream = urlConnection.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                result.append(line).append("\n");
            }

        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        }

        return result.toString();
    }


}
