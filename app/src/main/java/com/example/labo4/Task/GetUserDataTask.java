package com.example.labo4.Task;



import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.ListView;
import android.widget.Toast;


import com.example.labo4.ListActivity;
import com.example.labo4.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class GetUserDataTask extends  AsyncTask<String, Void, String> {


    Context context;
    ProgressDialog progressDialog;

    User user;
    String login;
    String password;



    public GetUserDataTask(Context context,String username,String password)
    {
        this.context = context;
        this.login = username;
        this.password = password;
        this.user = new User();
    }

    @Override
    protected void onPreExecute() {

        super.onPreExecute();

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {

        try {
            return getData(params[0]);
        } catch (IOException ex) {
            return "Network error !";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        //set data response to textView
        //mResult.setText(result);
        /// Toast.makeText(context,result,Toast.LENGTH_LONG).show();

        JSONArray json = null;
        try {
            json = new JSONArray(result.toString());
            for(int i=0; i < json.length(); i++)
            {

                JSONObject equip = json.getJSONObject(i);
                if(login.equals(equip.getString("username")) && password.equals(equip.getString("password")))
                {
                    this.user.setNom(equip.getString("nom"));
                    this.user.setPrenom(equip.getString("prenom"));
                    this.user.setUsername(equip.getString("username"));
                    this.user.setPassword(equip.getString("password"));

                    break;
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }



        //cancel progress dialog
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        if(this.user.getUsername() != null && this.user.getPassword() != null)
        {
            Intent intent = new Intent(context, ListActivity.class);
            intent.putExtra("User",this.user);
            context.startActivity(intent);
        }
        else
        {
            Toast.makeText(context,"Username or password not valid...",Toast.LENGTH_LONG).show();

        }
    }


    private String getData(String urlPath) throws IOException {
        StringBuilder result = new StringBuilder();
        BufferedReader bufferedReader =null;

        try {
            //Initialize and config request, then connect to server
            URL url = new URL(urlPath);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(10000 /* milliseconds */);
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("Content-Type", "application/json");// set header
            urlConnection.connect();

            //Read data response from server
            InputStream inputStream = urlConnection.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                result.append(line).append("\n");
            }

        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        }

        return result.toString();
    }


}
