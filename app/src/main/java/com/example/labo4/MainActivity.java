package com.example.labo4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.labo4.Task.GetUserDataTask;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    EditText username;
    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        username=(EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);


    }

    public void GoToList(View view) {
        GetUserDataTask user = new GetUserDataTask(MainActivity.this,username.getText().toString(),password.getText().toString());
        user.execute("http://192.168.0.170:4200/getDataUser");
    }


    public void moveTonext(View view) {
        Intent intent = new Intent(this,MainSingUpActivity.class);
        startActivity(intent);
    }
}