package com.example.labo4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.labo4.Task.GetSingleDataTask;

public class SingleDataActivity extends AppCompatActivity {


    Point point = new Point();
    TextView temp;
    TextView press;
    TextView date;
    TextView humi;
    TextView longi;
    TextView loti;
    EditText txt;

    ImageView arrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_data);

        arrow = (ImageView) findViewById(R.id.arrow);

        temp = (TextView) findViewById(R.id.temp);
        press = (TextView) findViewById(R.id.press);
        date = (TextView) findViewById(R.id.date);
        humi = (TextView) findViewById(R.id.humi);
        longi = (TextView) findViewById(R.id.longi);
        loti = (TextView) findViewById(R.id.luti);


        Bundle extra = getIntent().getExtras();
        this.point = (Point) extra.getSerializable("Object");







        //System.out.println("data task : "+dataTask.gettingData().getHumidite());
        //System.out.println("data task : "+this.point.getHumidite());



        temp.setText(this.point.getTemperature());
        press.setText(this.point.getPression() + " hPa");
        date.setText(this.point.getDate());
        humi.setText(this.point.getHumidite() + " %");
        longi.setText(this.point.getLongitude());
        loti.setText(this.point.getLutidue());
        int number = (int) Float.parseFloat(this.point.getTemperature());
        System.out.println("Value number :"+number);
        ExactRotaion(number);




    }


    public void ExactRotaion(int temp)
    {



        if(  0 < temp  && temp <= 15  )
        {
            int num = temp ;
            Rotation(0, (int) (-90 + temp));
        }

        if(  15 < temp  && temp <= 20  )
        {
            float num = temp + 5 ;
            Rotation(0, (int) (-90 + num));
        }


        if( 20 < temp && temp <= 25 )
        {
            float num = temp + 10;
            Rotation(0, (int) (-90 + num));
        }

        if( 25 < temp && temp <= 30 )
        {
            float num = temp+ 15;
            Rotation(0, (int) (-90 + num));
        }

        if( 30 < temp  && temp <= 35 )
        {
            float num = temp + 20;
            Rotation(0, (int) (-90 + num));
        }

        if( 35 < temp  && temp <= 40 )
        {
            float num = temp + 25;
            Rotation(0, (int) (-90 + num));
        }

        if( 40 < temp  && temp <= 45 )
        {
            float num = temp + 30;
            Rotation(0, (int) (-90 + num));
        }

        if( 45 < temp  && temp < 50 )
        {
            float num = temp + 35;
            Rotation(0, (int) (-90 + num));
        }

        if( temp ==50)
        {
            float num = temp + 35;
            Rotation(0,  0);
        }



        if( 50 < temp && temp <= 55 )
        {
            int num = temp;
            Rotation(0, (int) (num -45));
        }

        if( 55 < temp && temp <= 60 )
        {
            float num = temp;
            Rotation(0, (int) (num -40));
        }

        if( 60 < temp && temp <= 65 )
        {
            float num = temp;
            Rotation(0, (int) (num -35));
        }

        if( 65 < temp && temp <= 70 )
        {
            float num = temp;
            Rotation(0, (int) (num -30));
        }

        if( 70 < temp && temp <= 75 )
        {
            float num = temp;
            Rotation(0, (int) (num -25));
        }

        if( 75 < temp && temp <= 80 )
        {
            float num = temp;
            Rotation(0, (int) (num -20));
        }

        if( 80 < temp && temp <= 90 )
        {
            float num = temp;
            Rotation(0, (int) (num -15));
        }



        if( 90 < temp && temp <= 99 )
        {
            float num = temp;
            Rotation(0, (int) (num -10));
        }

        if(  temp == 100 )
        {
            float num = temp;
            Rotation(0,  90);
        }



    }

    public void Rotation(int x , int y)
    {
        RotateAnimation animation = new RotateAnimation(x,y, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setDuration(1000);
        animation.setInterpolator(new LinearInterpolator());

        arrow.startAnimation(animation);
        arrow.setRotation(y);
    }


}