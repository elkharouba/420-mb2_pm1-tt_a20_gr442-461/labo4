package com.example.labo4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.labo4.GPS.GPSLocationTracket;

import com.example.labo4.Task.GetDataTask;
import com.example.labo4.Task.GetSingleDataTask;
import com.example.labo4.Task.PostDataTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ListActivity extends AppCompatActivity implements SensorEventListener {


    //Sensor
    private SensorManager sm ;
    private Sensor sensorPression;
    private Sensor sensorTemp;
    private Sensor sensorHumidite;
    //End of Sensor

    //ListView
    ArrayList<Point> listnewsData;
    ListView lsNews;
    MyCustomAdapter myadapter;
    //End of ListView

    //Classes
    GPSLocationTracket gps;
    Point point;
    //End of Classes

    //Variables

    public  String Latitude;
    public String Longitude;
    public String Temperature;
    public String Pressure;
    public String Humidite;
    //End Of Variables



    //Setters and Getters
    /**
    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        this.Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        this.Longitude = longitude;
    }

    public String getTemperature() {
        return Temperature;
    }

    public void setTemperature(String temperature) {
        Temperature = temperature;
    }

    public String getPressure() {
        return Pressure;
    }

    public void setPressure(String pressure) {
        Pressure = pressure;
    }

    public String getHumidite() {
        return Humidite;
    }

    public void setHumidite(String humidite) {
        Humidite = humidite;
    }
     */
    //End of Setters and Getters


    Button  NewPoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        NewPoint =  (Button) findViewById(R.id.NewPoint);
        point = new Point();

        //Sensor
        sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        sensorPression = sm.getDefaultSensor(Sensor.TYPE_PRESSURE);
        sm.registerListener(this,sensorPression,SensorManager.SENSOR_DELAY_NORMAL);

        sensorHumidite  = sm.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
        sm.registerListener(this,sensorHumidite,SensorManager.SENSOR_DELAY_NORMAL);

        sensorTemp = sm.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        sm.registerListener(this,sensorTemp,SensorManager.SENSOR_DELAY_NORMAL);
        // End of Sensor



        // Calling Method chargeData
        chargeData();

        // Button add New Point to Serveur
        this.NewPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Calling Method addData
                AddData();
            }

        });


        // Function to Move TO Next Activity "Singe Data"
        lsNews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                GetSingleDataTask dataTask = new GetSingleDataTask(ListActivity.this, position);
                dataTask.execute("http://192.168.0.170:4200/getData");
            }
        });





    }

    // Method get Current Time
    public void getCurrentTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
        String currentDateandTime = sdf.format(new Date());
        point.setDate(currentDateandTime);
    }

    // Method get Current Location
    public void GetMyLocation()
    {
        gps = new GPSLocationTracket(ListActivity.this);

        if(gps.canGetLocation())
        {
            //setLatitude(String.valueOf(gps.getLatitude()));
            point.setLutidue(String.valueOf(gps.getLatitude()));
            //setLongitude(String.valueOf(gps.getLongitude()));
            point.setLongitude(String.valueOf(gps.getLongitude()));
        }else
        {
            gps.showSettingsAlert();
        }
    }

    // Method add data to serveur
    public void AddData()
    {
        GetMyLocation();
        getCurrentTime();
        PostDataTask postdata = new PostDataTask(ListActivity.this);
        //postdata.execute("http://192.168.0.170:4200/newPoint",getTemperature(),getPressure(),getHumidite(),getCurrentTime(),getLatitude(),getLongitude());
        postdata.execute("http://192.168.0.170:4200/newPoint",point.getTemperature(),point.getPression(),point.getHumidite(),point.getDate(),point.getLutidue(),point.getLongitude());

        //update  data in listview
        listnewsData.add(point);
        myadapter.notifyDataSetChanged();
    }

    // Method getting data from serveur
    public void chargeData() {

        GetDataTask task = new GetDataTask(this);
        task.execute("http://192.168.0.170:4200/getData");
        task.gettingData();
        listnewsData = new ArrayList<Point>();
        for (Point p : task.gettingData()) {
            System.out.println(p.get_id());
        }
        //add data and view it
        listnewsData = task.gettingData();
        myadapter = new MyCustomAdapter(task.gettingData());
        lsNews = (ListView) findViewById(R.id.listview);
        lsNews.setAdapter(myadapter);//intisal with data




    }

    // Methods Sensor
    @Override
    public void onSensorChanged(SensorEvent event) {

        if(event.sensor.getType() == Sensor.TYPE_PRESSURE)
        {

            //setPressure(String.valueOf(event.values[0]));
            point.setPression(String.valueOf(event.values[0]));
        }

        if(event.sensor.getType() == Sensor.TYPE_RELATIVE_HUMIDITY)
        {

            //setHumidite(String.valueOf(event.values[0]));
            point.setHumidite(String.valueOf(event.values[0]));
        }

        if(event.sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE)
        {
            //setTemperature(String.valueOf(event.values[0]));
            point.setTemperature(String.valueOf(event.values[0]));
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
    // End of  Methods Sensor


    // List Costume Adapter
    private class MyCustomAdapter extends BaseAdapter  {
        public  ArrayList<Point>  listnewsDataAdpater ;

        public MyCustomAdapter(ArrayList<Point>  listnewsDataAdpater) {
            this.listnewsDataAdpater=listnewsDataAdpater;
        }


        @Override
        public int getCount() {
            return listnewsDataAdpater.size();
        }

        @Override
        public String getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            LayoutInflater mInflater = getLayoutInflater();
            View myView = mInflater.inflate(R.layout.costum_list, null);

            final   Point s = listnewsDataAdpater.get(position);

            TextView myid = (TextView) myView.findViewById(R.id.myid);
            myid.setText(s.get_id());
            Button more = (Button) myView.findViewById(R.id.btnMore);
            TextView _date = (TextView) myView.findViewById(R.id.mydate);
            _date.setText(s.getDate());

            more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    GetSingleDataTask dataTask = new GetSingleDataTask(ListActivity.this, position);
                    dataTask.execute("http://192.168.0.170:4200/getData");
                }
            });

            return myView;

        }

    }







}